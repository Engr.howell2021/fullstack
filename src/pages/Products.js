import React, {useContext, useState, useEffect} from 'react'

import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

import UserContext from '../UserContext';

export default function Products() {

    const { user } = useContext(UserContext)
    const[allProducts, setAllProducts] = useState([])

    const fetchData = () => {
        fetch('https://sheltered-ridge-64247.herokuapp.com/products/allproducts')
        .then (res => res.json())
        .then (data => {
            console.log(data)
            setAllProducts(data)
        })
    }

    useEffect(() => {
        fetchData()
    },[])


    return(
        <>
            {(user.isAdmin === true) ?
             <AdminView productsData={allProducts} fetchData={fetchData}/>

             :

             <UserView productsData={allProducts}/>
            }
        </>
    ) 
}