import React from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Highlights(){
	return(
		<Row>
            <h1 className='text-center m-5'>The Latest</h1>
			<Col xs={12} md={3}>
				<Card className="cardHighlight p-3">
                <img
     className="d-block w-100" 
     src={require("../images/electric.png")} 
     width="px" 
     height="325px"
    />
					<Card.Body>
						<Card.Title>
							<h5>Electric Shoes</h5>
						</Card.Title>
					</Card.Body>
					<Button variant="dark"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
				</Card>
			</Col>
					
			<Col xs={12} md={3}>
				<Card className="cardHighlight p-3">
                <img
     className="d-block w-100" 
     src={require("../images/light.png")} 
     width="px" 
     height="325px"
    />
					<Card.Body>
						<Card.Title>
							<h5>Aorus Lightwing Rgb Limite Edition</h5>
						</Card.Title>
						
					</Card.Body>
					<Button variant="dark"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
				</Card>
			</Col>

			<Col xs={12} md={3}>
				<Card className="cardHighlight p-3">
                <img
     className="d-block w-100" 
     src={require("../images/white.png")} 
     width="px" 
     height="325px"
    />
					<Card.Body>
						<Card.Title>
							<h5>Stability Running Shoes</h5>
						</Card.Title>
					
					</Card.Body>
                    <Button variant="dark"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
				</Card>
			</Col>

			<Col xs={12} md={3}>
				<Card className="cardHighlight p-3">
                <img
     className="d-block w-100" 
     src={require("../images/blue.png")} 
     width="px" 
     height="325px"
    />
					<Card.Body>
						<Card.Title>
							<h5>Men's Epic React Flyknit Running shoes</h5>
						</Card.Title>
						
					</Card.Body>
                    <Button variant="dark"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
				</Card>
			</Col>
		</Row>
		);
}
